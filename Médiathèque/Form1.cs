﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Médiathèque.Model.Dao;
using Médiathèque.Model.Classes;

namespace Médiathèque
{
    public partial class Form1 : Form
    {
        List<Ouvrage> ouvrages;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OuvrageDao dao = new OuvrageDao();
            ouvrages = dao.GetAll();
            lstOuvrages.DataSource = ouvrages;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstOuvrages.DataSource != null)
            {
                Ouvrage ouvrage = (Ouvrage)lstOuvrages.SelectedItem;
                TxtIsbn.Text = ouvrage.Isbn;
                TxtTitle.Text = ouvrage.Titre;
                TxtDescription.Text = ouvrage.Description;

            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label_isbn(object sender, EventArgs e)
        {

        }

        private void TxtIsbn_TextChanged(object sender, EventArgs e)
        {

        }

        private void label_title(object sender, EventArgs e)
        {

        }

        private void TxtTitle_TextChanged(object sender, EventArgs e)
        {

        }

        private void label_description(object sender, EventArgs e)
        {

        }

        private void TxtDescription_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_Create(object sender, EventArgs e)
        {

        }

        private void btn_Update(object sender, EventArgs e)
        {
            string isbn = TxtIsbn.Text;
            string titre = TxtTitle.Text;
            string description = TxtDescription.Text;

            Ouvrage ouvrage = (Ouvrage)lstOuvrages.SelectedItem;
            OuvrageDao dao = new OuvrageDao();
            int nbRows = dao.Update(isbn, titre, description);

            if (nbRows == 1)
            {
                ouvrage.Isbn = isbn;
                ouvrage.Titre = titre;
                ouvrage.Description = description;

                lstOuvrages.DataSource = null;
                lstOuvrages.DataSource = ouvrages;
            }

            
        }

        private void btn_Delete(object sender, EventArgs e)
        {

        }
    }
}
